# The Maze Explorer

## Overview

The Maze explorer currently supports mazes solvable by the walk along the wall algorithm (left first)

The Maze explorer, will start at the start node, and try to go left, if the explorer cannot, they will try straight on, 
if they cannot, they will try right, and failing that they will turn around and go backwards.

## Usage

Implement the Wall Follower Maze Explorer like so:

    Maze maze = new Maze(new File("/path/to/mazefile")); 
    WallFollowerExplorer explorer = new WallFollowerExplorer(maze);
    explorer.exploreMaze();

For a list of example mazes see src/main/test/resources.

A Maze (as defined in src/main/test/resources/WallFollowerExplorer/Maze1.txt) consists of walls 'X', Empty spaces ' ', 
one and only one Start point 'S' and one and only one exit 'F'. 
 
To print the route that was taken by the Explorer:

    System.out.println(explorer.getRoute().toString())

# Development

Clone:

    git clone git@bitbucket.org:PRS1/maze.git

Run the tests:

    mvn test

# Implementation Notes 

## Design

I have not gone for a coordinate based system, because I felt it reflected the real way an explorer might explore the maze.
An explorer could be given just the start Node, and be able to figure out the maze by traversing node to node, which I felt
represents a real explorer! I have still made use of coordinates in order to fulfil the requirements.

## Assumptions

These are the assumptions made throughout development:

 * Each row's length in the maze is the same as one another, and the same goes for columns. 

 * Only mazes solvable by the wall follower algorithm are currently supported

 * The objective of the explorer is to find and plot a single route through the maze. The following features are not supported:
  
    - Finding the most efficient route
    - Finding all possible routes
    - Plotting orientation travelled at each route
 
 * The terminology 'space' does not include start and finish points.