package uk.gov.dwp.maze;

import java.util.*;

public class Bearing {
    private static final List<Orientation> bearings = new ArrayList<>(Arrays.asList(Orientation.UP, Orientation.RIGHT, Orientation.DOWN, Orientation.LEFT));

    public static Orientation getBearing(Orientation currentOrientation, Orientation direction) {
        int currentBearing = bearings.indexOf(currentOrientation);
        int direcitonBearing = bearings.indexOf(direction);
        return bearings.get((currentBearing + direcitonBearing) % bearings.size());
    }

}