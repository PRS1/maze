package uk.gov.dwp.maze;

/**
 * Created by williamlacy on 03/10/16.
 */
public class CoordXY {
    private final int x;
    private final int y;

    public CoordXY(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public int getY() {
        return y;
    }

    public int getX() {
        return x;
    }
}
