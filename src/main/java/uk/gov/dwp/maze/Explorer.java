package uk.gov.dwp.maze;

public abstract class Explorer {

    protected final Maze maze;

    protected Route route;

    protected boolean finished = false;

    protected MazeNode currentNode;

    protected Orientation currentOrientation;

    public Explorer(Maze m) {
        this.maze = m;
        this.currentNode = maze.getStartNode();
        this.route = new Route(maze.getXWidth(), maze.getYLength());
    }

    /**
     * explore the Maze
     *
     * @return the finish node
     * @throws UnsolvableMaze
     */
    public abstract MazeNode exploreMaze() throws UnsolvableMaze;

    /**
     * Move explorer forwards in the direction of current orientation and plot the move.
     *
     * @return true if move was successful and false if not, ie a wall is in the way.
     */
    public boolean move() {
        MazeNode newNode = discoverNextNode(Orientation.UP);
        if (!newNode.isWall()) {
            currentNode = newNode;
            route.plot(currentNode.getXyCoord());
            return true;
        }
        return false;
    }

    /**
     * get the node that is in the direction from current orientation
     *
     * @param direction: the direction of the node to get compared to the current orientation
     * @return maze node in the given direction
     */
    public MazeNode discoverNextNode(Orientation direction) {
        return currentNode.getNeighbour(Bearing.getBearing(currentOrientation, direction));
    }

    /**
     * turn current orientation to the given direction
     * @param direction
     */
    public void turn(Orientation direction) {
        currentOrientation = Bearing.getBearing(currentOrientation, direction);
    }

    public MazeNode getCurrentNode() {
        return currentNode;
    }

    public boolean isFinished() {
        return finished;
    }

    public Orientation getCurrentOrientation() {
        return currentOrientation;
    }

    public Route getRoute() {
        return route;
    }

    public class UnsolvableMaze extends Exception {
        public UnsolvableMaze(String message) {
            super(message);
        }
    }


}
