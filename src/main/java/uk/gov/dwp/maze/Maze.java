package uk.gov.dwp.maze;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;
import java.util.Scanner;

public class Maze {

    private MazeNode startNode;

    private MazeNode finishNode;

    private File mazeFile;

    private List<List<MazeNode>> linkedMazeGrid;

    private int totalWalls;

    private int totalSpaces;

    public Maze(File mazeFile) throws MazeException {
        this.mazeFile = mazeFile;
        generateMaze();
    }

    /**
     * generates the maze form the maze file.
     * @throws MazeException for:
     *    - unrecognised characters, X, F, S and spaces are valid
     *    - too many start (S) nodes
     *    - too many finish (F) nodes
     *    - no start node
     *    - no finish node
     */
    private void generateMaze() throws MazeException {
        totalWalls = totalSpaces = 0;
        startNode = finishNode = null;
        Scanner s;
        try {
            s = new Scanner(mazeFile);
        } catch (FileNotFoundException e) {
            throw new MazeException(e.getMessage());
        }
        s.useDelimiter("\n");
        ArrayList<String> mazeFileRows = new ArrayList<>();
        while (s.hasNext()) {
            mazeFileRows.add(s.next());
        }
        s.close();
        ListIterator li = mazeFileRows.listIterator(mazeFileRows.size());
        linkedMazeGrid = new ArrayList<>();
        // iterate in reverse order so that coordinates start at the bottom left of the map instead of top left
        while (li.hasPrevious()) {
            String mazeRow = String.valueOf(li.previous());
            List<MazeNode> mazeY = new ArrayList<>();
            for (char mazeNodeChar : mazeRow.toCharArray()) {
                MazeNode n = new MazeNode();
                if (mazeNodeChar == 'S') {
                    if (startNode != null)
                        throw new MazeException("Too many start nodes. Mazes can only contain 1");
                    startNode = n;
                    n.setStart(true);
                } else if (mazeNodeChar == 'F') {
                    if (finishNode != null)
                        throw new MazeException("Too many finish nodes. Mazes can only contain 1");
                    finishNode = n;
                    n.setFinish(true);
                } else if (mazeNodeChar == 'X') {
                    totalWalls++;
                    n.setWall(true);
                } else if (mazeNodeChar == ' ') {
                    totalSpaces++;
                } else {
                    throw new MazeException("invalid maze character, must contain X, F, X or S");
                }
                mazeY.add(n);
            }
            linkedMazeGrid.add(mazeY);
        }
        if (finishNode == null || startNode == null)
            throw new MazeException("Invalid maze. Mazes must contain at least 1 start and 1 finish node");
        int y = 0;
        for (List<MazeNode> mazeNodesY : linkedMazeGrid) {
            int x = 0;
            for (MazeNode n : mazeNodesY) {
                n.setXyCoord(new CoordXY(x, y));
                if (inbounds(mazeNodesY, x - 1)) n.setNeighbourNode(Orientation.LEFT, mazeNodesY.get(x - 1));
                if (inbounds(mazeNodesY, x + 1)) n.setNeighbourNode(Orientation.RIGHT, mazeNodesY.get(x + 1));
                if (inbounds(linkedMazeGrid, y + 1))
                    n.setNeighbourNode(Orientation.UP, linkedMazeGrid.get(y + 1).get(x));
                if (inbounds(linkedMazeGrid, y - 1))
                    n.setNeighbourNode(Orientation.DOWN, linkedMazeGrid.get(y - 1).get(x));
                x++;
            }
            y++;
        }
    }

    /**
     *
     * @param l: list to check
     * @param i: index
     * @return: true if the index is in the list
     */
    private boolean inbounds(List l, int i) {
        return (i >= 0) && (i < l.size());
    }

    /**
     * returns the node at the given x and y coords where x and y are 0 at the bottom left node of the map.
     * e.g. getTotalSpaces(2,1) returns this node:
     * 2 XFX
     * 1 X X<- this one
     * 0 XSX
     * ycoords ^
     * xcoords  >012
     *
     * @param coord: the xy coordinate
     * @return: MazeNode at given x and y coords
     */
    public MazeNode getNodeByCoord(CoordXY coord) {
        // Y is the containing list, due to the way that the maze file is read.
        return linkedMazeGrid.get(coord.getY()).get(coord.getX());
    }

    /**
     *
     * @return the width along the x-axis of the maze
     */
    public int getXWidth() {
        // assumes that every row is of equal length
        return linkedMazeGrid.get(0).size();
    }

    /**
     *
     * @return the length along the y-axis of the maze
     */
    public int getYLength() {
        // assumes that every row is of equal length
        return linkedMazeGrid.size();
    }

    public MazeNode getStartNode() {
        return startNode;
    }

    public int getTotalWalls() {
        return totalWalls;
    }

    public int getTotalSpaces() {
        return totalSpaces;
    }

    public class MazeException extends Exception {
        public MazeException(String message) {
            super(message);
        }
    }

}
