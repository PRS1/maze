package uk.gov.dwp.maze;

import java.util.HashMap;

public class MazeNode {
    private boolean start;
    private boolean finish;
    private boolean wall;
    private CoordXY xyCoord;

    public void setStart(boolean start) {
        this.start = start;
    }

    public void setFinish(boolean finish) {
        this.finish = finish;
    }

    public void setWall(boolean wall) {
        this.wall = wall;
    }

    public void setNeighbourNode(Orientation o, MazeNode n) {
        this.neighbourNodes.put(o, n);
    }

    public MazeNode() {
        this.neighbourNodes = new HashMap<>();
    }

    private HashMap<Orientation, MazeNode> neighbourNodes;

    public boolean isStart() {
        return start;
    }

    public boolean isFinish() {
        return finish;
    }

    public boolean isWall() {
        return wall;
    }

    /**
     * returns neighboring node in given orientation to current node
     *
     * @param orientation: assumes nodes themselves are oriented UP, returns the given node
     * @return MazeNode
     */
    public MazeNode getNeighbour(Orientation orientation) {
        return this.neighbourNodes.get(orientation);
    }

    public CoordXY getXyCoord() {
        return xyCoord;
    }

    public void setXyCoord(CoordXY xyCoord) {
        this.xyCoord = xyCoord;
    }

}
