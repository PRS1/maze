package uk.gov.dwp.maze;

public enum Orientation {
    UP, RIGHT, DOWN, LEFT
}
