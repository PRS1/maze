package uk.gov.dwp.maze;

import java.util.ArrayList;
import java.util.List;

public class Route {

    private final int sizeX;
    private final int sizeY;
    private List<CoordXY> route;

    public Route(int sizeX, int sizeY) {
        this.sizeX = sizeX;
        this.sizeY = sizeY;
        this.route = new ArrayList<>();
    }

    /**
     * plots the visited coordinate on the route
     * @param coord: the xy coord that was visited
     */
    public void plot(CoordXY coord) {
        route.add(coord);
    }

    /**
     * prints the plotted route. '*'s represent visited nodes of the map and '-'s represent not visited.
     * @return String
     */
    @Override
    public String toString() {
        Boolean[][] grid = new Boolean[sizeX][sizeY];
        for (CoordXY coord : route) {
            grid[coord.getX()][coord.getY()] = true;
        }
        StringBuilder sb = new StringBuilder();
        for (Boolean[] row : grid) {
            for (Boolean isPointVisited : row) {
                if (isPointVisited != null && isPointVisited) {
                    sb.append("*");
                } else {
                    sb.append("-");
                }
            }
            sb.append(System.getProperty("line.separator"));
        }
        return sb.toString();
    }

}
