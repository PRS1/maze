package uk.gov.dwp.maze;

import java.util.*;

/**
 * Created by williamlacy on 27/09/16.
 */
public class WallFollowerExplorer extends Explorer {

    private List<Orientation> tryDirectionOrder = Arrays.asList(Orientation.LEFT, Orientation.UP, Orientation.RIGHT, Orientation.DOWN);

    public WallFollowerExplorer(Maze m) {
        super(m);
        this.currentOrientation = Orientation.UP;
    }

    private MazeNode exploreCurrentNode() throws UnsolvableMaze {
        for (Orientation direction : tryDirectionOrder) {
            MazeNode nodeInDirection = currentNode.getNeighbour(Bearing.getBearing(currentOrientation, direction));
            if (nodeInDirection != null) {
                //not an edge
                if (nodeInDirection.isFinish()) {
                    finished = true;
                    return currentNode;
                } else if (nodeInDirection.isStart()) {
                    throw new UnsolvableMaze("No route to finish. Ended up back at the start.");
                } else if (!nodeInDirection.isWall()) {
                    turn(direction);
                    move();
                    return exploreCurrentNode();
                }
            }
        }
        throw new UnsolvableMaze("Got stuck with nowhere to go at: " + currentNode.getXyCoord().toString());
    }

    @Override
    public MazeNode exploreMaze() throws UnsolvableMaze {
        currentNode = maze.getStartNode();
        currentOrientation = Orientation.UP;
        route = new Route(maze.getXWidth(), maze.getYLength());
        return exploreCurrentNode();
    }
}
