import org.junit.Test;
import uk.gov.dwp.maze.Maze;
import uk.gov.dwp.maze.MazeNode;
import uk.gov.dwp.maze.Orientation;
import uk.gov.dwp.maze.CoordXY;

import java.io.File;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

public class MazeTest {

    // test maze constructor
    @Test
    public void shouldCreateAValidMaze() throws Maze.MazeException {
        Maze m = new Maze(loadTestResource("Maze/smallMaze.txt"));
        MazeNode nodex1y0 = m.getStartNode();
        assertThat(nodex1y0.isStart(), is(equalTo(true)));
        assertThat(nodex1y0.isFinish(), is(equalTo(false)));
        assertThat(nodex1y0.isWall(), is(equalTo(false)));
        assertThat(nodex1y0.getNeighbour(Orientation.DOWN), is(nullValue()));
        MazeNode nodex0y0 = nodex1y0.getNeighbour(Orientation.LEFT);
        assertThat(nodex0y0.isWall(), is(true));
        MazeNode nodex2y0 = nodex1y0.getNeighbour(Orientation.RIGHT);
        assertThat(nodex2y0.isWall(), is(true));
        MazeNode nodex1y1 = nodex1y0.getNeighbour(Orientation.UP);
        assertThat(nodex1y1.isWall(), is(false));
        assertThat(nodex1y1.isFinish(), is(false));
        assertThat(nodex1y1.isStart(), is(false));
        assertThat(nodex1y1.getNeighbour(Orientation.DOWN), is(equalTo(nodex1y0)));
        MazeNode nodex0y1 = nodex1y1.getNeighbour(Orientation.LEFT);
        assertThat(nodex0y1.isWall(), is(true));
        MazeNode nodex2y1 = nodex1y1.getNeighbour(Orientation.RIGHT);
        assertThat(nodex2y1.isWall(), is(true));
        MazeNode nodex1y2 = nodex1y1.getNeighbour(Orientation.UP);
        assertThat(nodex1y2.isWall(), is(false));
        assertThat(nodex1y2.isFinish(), is(equalTo(true)));
        assertThat(nodex1y2.isStart(), is(equalTo(false)));
        MazeNode nodex0y2 = nodex1y2.getNeighbour(Orientation.LEFT);
        assertThat(nodex0y2.isWall(), is(true));
        MazeNode nodex2y2 = nodex1y2.getNeighbour(Orientation.RIGHT);
        assertThat(nodex2y2.isWall(), is(true));
        assertThat(nodex2y2.getNeighbour(Orientation.UP), is(nullValue()));
    }

    @Test
    public void shouldProvideTotalNumberOfWallsAndSpaces() throws Maze.MazeException {
        Maze m = new Maze(loadTestResource("Maze/smallMaze.txt"));
        assertThat(m.getTotalWalls(), is(equalTo(6)));
        assertThat(m.getTotalSpaces(), is(equalTo(1)));
    }

    @Test(expected = Maze.MazeException.class)
    public void shouldThrowMazeErrorWhenMoreThanOneFinishPointsAreProvided() throws Maze.MazeException {
        new Maze(loadTestResource("Maze/invalidMazeTooManyFinish.txt"));
    }

    @Test(expected = Maze.MazeException.class)
    public void shouldThrowMazeErrorWhenMoreThanOneStartPointsAreProvided() throws Maze.MazeException {
        new Maze(loadTestResource("Maze/invalidMazeTooManyStart.txt"));
    }

    @Test(expected = Maze.MazeException.class)
    public void shouldThrowMazeErrorWhenNoStartPointIsProvided() throws Maze.MazeException {
        new Maze(loadTestResource("Maze/invalidMazeNoStart.txt"));
    }

    @Test(expected = Maze.MazeException.class)
    public void shouldThrowMazeErrorWhenNoFinishPointIsProvided() throws Maze.MazeException {
        new Maze(loadTestResource("Maze/invalidMazeNoFinish.txt"));
    }

    @Test(expected = Maze.MazeException.class)
    public void shouldThrowMazeErrorWhenInvalidMazeCharFound() throws Maze.MazeException {
        new Maze(loadTestResource("Maze/invalidMaze.txt"));
    }

    @Test(expected = Maze.MazeException.class)
    public void shouldThrowMazeErrorCannotFindFile() throws Maze.MazeException {
        try {
            new Maze(new File(""));
        } catch (Maze.MazeException e) {
            assertThat(e.getMessage(), containsString("No such file"));
            throw e;
        }
    }

    @Test()
    public void shouldReturnTheNodeAtACoordinate() throws Maze.MazeException {
        /** How x and y coords work
         2 XFX
         1 X X<- this one
         0 XSX
         ycoords ^
         xcoords  >012
         */
        Maze m = new Maze(loadTestResource("Maze/smallMaze.txt"));
        MazeNode expectedNode = m.getStartNode().getNeighbour(Orientation.UP).getNeighbour(Orientation.RIGHT);
        MazeNode n = m.getNodeByCoord(new CoordXY(2, 1));
        assertThat(n, is(equalTo(expectedNode)));
    }


    private File loadTestResource(String path) {
        ClassLoader classLoader = getClass().getClassLoader();
        return new File(classLoader.getResource(path).getFile());
    }
}

