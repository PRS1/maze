import org.junit.Test;
import uk.gov.dwp.maze.*;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;

public class WallFollowerExplorerTest {
    @Test()
    public void shouldBeAbleToDropInAtTheStartOfTheMaze() throws Maze.MazeException {
        Maze m = new Maze(loadTestResource("WallFollowerExplorer/Maze1.txt"));
        WallFollowerExplorer explorer = new WallFollowerExplorer(m);
        assertThat(explorer.getCurrentNode(), is(equalTo(m.getStartNode())));
    }

    @Test()
    public void shouldBeAbleToMoveForward() throws Maze.MazeException {
        WallFollowerExplorer explorer = new WallFollowerExplorer(new Maze(loadTestResource("WallFollowerExplorer/Maze1.txt")));
        explorer.turn(Orientation.RIGHT);
        assertThat(explorer.getCurrentNode().getXyCoord().getX(), is(equalTo(3)));
        assertThat(explorer.getCurrentNode().getXyCoord().getY(), is(equalTo(11)));
        assertThat(explorer.move(), is(equalTo(true)));
        assertThat(explorer.getCurrentNode().getXyCoord().getX(), is(equalTo(4)));
        assertThat(explorer.getCurrentNode().getXyCoord().getY(), is(equalTo(11)));
    }

    @Test()
    public void shouldNotMoveForwardWhenThereIsAWallAhead() throws Maze.MazeException {
        WallFollowerExplorer explorer = new WallFollowerExplorer(new Maze(loadTestResource("WallFollowerExplorer/Maze1.txt")));
        explorer.turn(Orientation.LEFT);
        assertThat(explorer.getCurrentNode().getXyCoord().getX(), is(equalTo(3)));
        assertThat(explorer.getCurrentNode().getXyCoord().getY(), is(equalTo(11)));
        assertThat(explorer.move(), is(equalTo(false)));
        assertThat(explorer.getCurrentNode().getXyCoord().getX(), is(equalTo(3)));
        assertThat(explorer.getCurrentNode().getXyCoord().getY(), is(equalTo(11)));
    }

    @Test()
    public void shouldBeAbleToTurnLeftAndRight() throws Maze.MazeException {
        WallFollowerExplorer explorer = new WallFollowerExplorer(new Maze(loadTestResource("WallFollowerExplorer/Maze1.txt")));
        assertThat(explorer.getCurrentOrientation(), is(equalTo(Orientation.UP)));
        explorer.turn(Orientation.LEFT);
        assertThat(explorer.getCurrentOrientation(), is(equalTo(Orientation.LEFT)));
        explorer.turn(Orientation.RIGHT);
        assertThat(explorer.getCurrentOrientation(), is(equalTo(Orientation.UP)));
        explorer.turn(Orientation.RIGHT);
        assertThat(explorer.getCurrentOrientation(), is(equalTo(Orientation.RIGHT)));
    }

    @Test()
    public void shouldBeAbleToUnderstandWhatIsInFrontOfThem() throws Maze.MazeException {
        WallFollowerExplorer explorer = new WallFollowerExplorer(new Maze(loadTestResource("WallFollowerExplorer/Maze1.txt")));
        MazeNode nodeUp = explorer.discoverNextNode(Orientation.UP);
        assertCoordinates(nodeUp, new CoordXY(3, 12));
        assertThat(nodeUp.isWall(), is(equalTo(true)));
    }

    @Test()
    public void shouldBeAbleToUnderstandWhatIsInAnyDirection() throws Maze.MazeException {
        WallFollowerExplorer explorer = new WallFollowerExplorer(new Maze(loadTestResource("WallFollowerExplorer/Maze1.txt")));
        MazeNode nodeLeft = explorer.discoverNextNode(Orientation.LEFT);
        assertCoordinates(nodeLeft, new CoordXY(2, 11));
        assertThat(nodeLeft.isWall(), is(equalTo(true)));
        MazeNode nodeDown = explorer.discoverNextNode(Orientation.DOWN);
        assertCoordinates(nodeDown, new CoordXY(3, 10));
        assertThat(nodeDown.isWall(), is(equalTo(true)));
        MazeNode nodeRight = explorer.discoverNextNode(Orientation.RIGHT);
        assertCoordinates(nodeRight, new CoordXY(4, 11));
        assertThat(nodeRight.isWall(), is(equalTo(false)));
    }

    @Test
    public void shouldFindFinish() throws Maze.MazeException, Explorer.UnsolvableMaze {
        WallFollowerExplorer explorer = new WallFollowerExplorer(new Maze(loadTestResource("WallFollowerExplorer/Maze1.txt")));
        explorer.exploreMaze();
        assertThat(explorer.isFinished(), is(equalTo(true)));
    }

    @Test(expected = Explorer.UnsolvableMaze.class)
    public void shouldThrowCannotSolveErrorWhenNoRouteToFinish() throws Maze.MazeException, Explorer.UnsolvableMaze {
        try {
            WallFollowerExplorer wallFollwerExplorer = new WallFollowerExplorer(new Maze(loadTestResource("WallFollowerExplorer/unsolveableMaze.txt")));
            wallFollwerExplorer.exploreMaze();
        } catch (Explorer.UnsolvableMaze e) {
            assertThat(e.getMessage(), containsString("No route to finish"));
            throw e;
        }
    }

    @Test
    public void shouldHaveARecordOfWhereTheyHaveBeenWhenFinishedSuccessfully() throws Maze.MazeException, Explorer.UnsolvableMaze, IOException {
        WallFollowerExplorer explorer = new WallFollowerExplorer(new Maze(loadTestResource("WallFollowerExplorer/Maze1.txt")));
        explorer.exploreMaze();
        assertThat(explorer.getRoute().toString().trim(), is(equalTo(fileToString(loadTestResource("WallFollowerExplorer/Maze1-expected-route.txt")).trim())));
    }

    @Test
    public void shouldHaveARecordOfWhereTheyHaveBeenWhenFailedToFinish() throws Maze.MazeException, IOException {
        WallFollowerExplorer explorer = new WallFollowerExplorer(new Maze(loadTestResource("WallFollowerExplorer/unsolveableMaze.txt")));
        try {
            explorer.exploreMaze();
        } catch (Explorer.UnsolvableMaze e) {
            assertThat(explorer.getRoute().toString().trim(), is(equalTo(fileToString(loadTestResource("WallFollowerExplorer/unsolveableMaze-expected-route.txt")).trim())));
        }
    }

    private String fileToString(File f) throws IOException {
        byte[] encoded = Files.readAllBytes(f.toPath());
        return new String(encoded, StandardCharsets.UTF_8);
    }

    private File loadTestResource(String path) {
        ClassLoader classLoader = getClass().getClassLoader();
        return new File(classLoader.getResource(path).getFile());
    }

    private void assertCoordinates(MazeNode n, CoordXY c) {
        assertThat(n.getXyCoord().getX(), is(equalTo(c.getX())));
        assertThat(n.getXyCoord().getY(), is(equalTo(c.getY())));
    }
}
